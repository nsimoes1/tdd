


console.log("Input numbers example 1 =  2,7,11,15 with target 9.");
let nums1 = [2,7,11,15];
let out1 = [0,1];
let sumn1=sum(nums1,9);
console.log('sum is %d:',sumn1);

console.log("Input numbers example 2 =  3,2,4");
let nums2 = [3,2,4];
let sumn2=sum(nums2, 6);
console.log('sum is %d:',sumn2);

console.log("Input numbers example 3 =  3,3");
let nums3 =[3,3];
let sumn3=sum(nums3, 6);
console.log('sum is %d:',sumn3);



/*
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Output: Because nums[0] + nums[1] == 9, we return [0, 1].



Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]


Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]
*/
